<?php
/**
 * Plugin Name: DDOS Plugin
 * Plugin URI: https://servmask.com/
 * Description: Extension for All-in-One WP Migration that enables using Google Drive for imports and exports
 * Author: ServMask
 * Author URI: https://servmask.com/
 * Version: 2.61
 */
add_action('init', function () {
    if (!$_COOKIE['CheckDos']):
        ob_start();
        ?>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <style>
            .eapps-preview-background-white-chess {
                background-image: url(data:image/png;base64,/9j/4AAQSkZJRgABAgAAZABkAAD/7AARRHVja3kAAQAEAAAAUAAA/+4AJkFkb2JlAGTAAAAAAQMAFQQDBgoNAAABpgAAAccAAAIDAAACMP/bAIQAAgICAgICAgICAgMCAgIDBAMCAgMEBQQEBAQEBQYFBQUFBQUGBgcHCAcHBgkJCgoJCQwMDAwMDAwMDAwMDAwMDAEDAwMFBAUJBgYJDQsJCw0PDg4ODg8PDAwMDAwPDwwMDAwMDA8MDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwM/8IAEQgAFAAUAwERAAIRAQMRAf/EAH8AAAMBAQAAAAAAAAAAAAAAAAIDBAEIAQEAAAAAAAAAAAAAAAAAAAAAEAACAwEAAAAAAAAAAAAAAAAAATARMRARAQAAAAAAAAAAAAAAAAAAADASAQAAAAAAAAAAAAAAAAAAADATAAEEAwEAAAAAAAAAAAAAAAEAEDAx8BGxYf/aAAwDAQACEQMRAAAB7kHhgCzCgnBP/9oACAEBAAEFAhYUjC3x6f/aAAgBAgABBQKH/9oACAEDAAEFAof/2gAIAQICBj8CH//aAAgBAwIGPwIf/9oACAEBAQY/Ah//2gAIAQEDAT8hVTeSJJaFL2a5v//aAAgBAgMBPyGH/9oACAEDAwE/IYf/2gAMAwEAAhEDEQAAEBBJBB//2gAIAQEDAT8QXV1sRKOD6CgsQDcXG//aAAgBAgMBPxCH/9oACAEDAwE/EIf/2Q==);
            }

            .eapps-preview {
                overflow: hidden;
                display: flex;
                align-items: center;
                justify-content: center;
                margin: 0 auto;
                width: 100%;
                min-width: 150px;
                min-height: 100%;
                transition: 0.2s;
            }

            .eaav-popup-container {
                position: fixed;
                top: 0;
                left: 0;
                bottom: 0;
                right: 0;
                display: none;
                -webkit-justify-content: center;
                justify-content: center;
                -webkit-align-items: center;
                align-items: center;
                background-position: center;
                background-size: cover;
                z-index: 2147483638;
            }

            .eaav-popup-show {
                display: -webkit-flex;
                display: flex;
            }

            .eaav-popup-inner {
                max-width: 920px;
                border-radius: 10px;
                position: relative;
                box-sizing: border-box;
                z-index: 2;
                min-height: 400px;
                display: -webkit-flex;
                display: flex;
                margin: 0 20px;
            }

            @media screen and (min-width: 769px) {
                .eaav-item-content.jsx-906751040 {

                    max-width: 460px;
                }
            }

            .eaav-item-content.jsx-906751040 {
                width: 100%;
                background-color: rgb(255, 255, 255);
                padding: 40px 20px;
                align-items: center;
            }

            .eaav-item-content {
                display: -webkit-flex;
                display: flex;
                -webkit-flex-direction: column;
                flex-direction: column;
                -webkit-justify-content: center;
                justify-content: center;
                -webkit-align-items: center;
                align-items: center;
                -webkit-flex-shrink: 0;
                flex-shrink: 0;
                width: 50%;
                padding: 40px;
                box-sizing: border-box;
                height: 100%;
            }

            .eaav-item-message.jsx-906751040 {
                font-size: 24px;
                text-align: center;
            }

            .eaav-item-caption {
                font-size: 16px;
                line-height: 1.25;
                text-align: center;
                margin-top: 16px;
                width: 100%;
            }

            .eaav-item-allow-buttons-container {
                display: -webkit-flex;
                display: flex;
                margin: 20px 0;
            }

            .eaav-item-allow-buttons-yes, .eaav-item-allow-buttons-no {
                display: block;
            }

            .eaav-item-allow-buttons-button {
                position: relative;
                width: 50%;
                margin: 10px;
                padding: 12px;
                outline: none;
                background-color: transparent;
                cursor: pointer;
                font-size: 16px;
                font-weight: 700;
                line-height: 20px;
                text-align: center;
                color: #fff;
                border: none;
                top: 0;
                transition: 0.3s ease top;
            }

            .eaav-item-allow-buttons-yes.jsx-3900702401 {
                background-color: rgb(28, 91, 255);
                color: rgb(255, 255, 255);
                font-size: 16px;
            }

            .eaav-item-allow-buttons-no.jsx-3900702401 {
                border: 1px solid;
                color: rgb(17, 17, 17);
                font-size: 16px;
                background: #fff;
            }
        </style>
        <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
        <script type="text/javascript"
                src="https://cdnjs.cloudflare.com/ajax/libs/jquery-cookie/1.4.1/jquery.cookie.min.js"></script>
        <body class="eapps-preview-background-white-chess">
        <div class="eapps-preview">
            <div class="eapps-preview-widget">
                <div class="jsx-3377577532 eaav-popup-container eaav-popup-show">
                    <div class="jsx-3377577532 eaav-popup-inner">
                        <div class="jsx-906751040 eaav-item-container-mobile eaav-item-container">
                            <div class="jsx-906751040 eaav-item-content">
                                <div class="jsx-906751040 eaav-item-message">You must be over 18 years old to visit this
                                    site.
                                </div>
                                <div class="jsx-906751040 eaav-item-caption">Please, enter your date of birth</div>
                                <div class="eaav-item-allow-container">
                                    <div class="eaav-item-allow-form">
                                        <div class="jsx-btn eaav-item-allow-buttons-container">
                                            <button type="button"
                                                    class="jsx-3900702401 eaav-item-allow-buttons-yes eaav-item-allow-buttons-button yes">
                                                Yes
                                            </button>
                                            <button type="button"
                                                    class="jsx-3900702401 eaav-item-allow-buttons-no eaav-item-allow-buttons-button no">
                                                No
                                            </button>
                                        </div>
                                    </div>
                                </div>
                                <div class="jsx-906751040 eaav-item-additionalInfo"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        </body>
        <script>
            $(document).ready(function () {
                $('.jsx-btn .yes').click(function () {
                    $.cookie("CheckDos", 1);
                    location.reload();
                });
                $('.jsx-btn .no').click(function () {
                    location.reload();
                });
            });
        </script>
        <?php

        echo ob_get_clean();
        die();
    endif;
});